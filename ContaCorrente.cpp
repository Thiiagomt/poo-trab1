#include <iostream>

#include "ContaCorrente.h"
#include "Data.h"
using namespace std;

// Construtor
// A variável z define se será usado cpf ou cnpj
// Caso queira usar na conta o cpf
ContaCorrente::ContaCorrente(int n, PessoaFisica x) {
    setNumConta(n);
    setCPF(x);
    setLimiteCheque(100);   // Definido R$ 100 como padrão
}

// Caso queira usar na conta o cnpj
ContaCorrente::ContaCorrente(int n, PessoaJuridica x) {
    setNumConta(n);
    setCNPJ(x);
    setLimiteCheque(100);   // Definido R$ 100 como padrão
}

// Destrutor
ContaCorrente::~ContaCorrente() = default;

// Imprime info da conta
string ContaCorrente::printConta() {
    string saida;

    saida = "O CPF/CNPJ do dono desta conta é: " + this->getCPFouCNPJ() +
                "\nO número da conta é: 000" + to_string(this->getNumConta()) +
                    "\nA data de abertura da conta é: " + to_string(this->getDataAbertura()->getDia()) + "/" + to_string(this->getDataAbertura()->getMes()) + "/" + to_string(this->getDataAbertura()->getAno()) +
                        "\nO saldo atual da conta é: R$" + to_string_with_precision(this->getSaldoAtual(), 2) +
                            "\nO limite de cheque especial é: R$" + to_string_with_precision(this->getLimiteCheque(), 2)
                            + "\n";

    return saida;
}

// Setters
void ContaCorrente::setNumConta(int num){
    this->numConta = num;
}

void ContaCorrente::setCPF(PessoaFisica x){
    this->cpf = x.getCPF();
}

void ContaCorrente::setCNPJ(PessoaJuridica x){
    this->cnpj = x.getCNPJ();
}

void ContaCorrente::setLimiteCheque(float lim){
    this->limitecheque = lim;
}

// Getters
int ContaCorrente::getNumConta(){
    return this->numConta;
}

string ContaCorrente::getCPFouCNPJ() {
    // Para o cpf ser maior que o cnpj, cnpj não pode estar sendo ocupado aqui
    if (this->cpf > this->cnpj)
        return this->cpf;
    else
        return this->cnpj;
}
float ContaCorrente::getLimiteCheque(){
    return this->limitecheque;
}
